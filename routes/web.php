<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();
Route::middleware('auth')->group(function () {
Route::resource('/license', 'LicenseController');
Route::post('license/key','LicenseController@exp_date')->name('license.exp_date');
Route::post('key/update','LicenseController@key_save')->name('license.key_save');
Route::post('key_update_save','LicenseController@key_update')->name('license.key_update');
});



