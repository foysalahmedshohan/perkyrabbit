@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
               

                <div class="card-body" style="background-color: #9cc16d">
                    <h3>Enter License Key </h3><br>
                    <form method="POST" action="{{ route('license.key_update') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="licenseKey" class="col-md-4 col-form-label text-md-right">{{ __('License Key') }}</label>

                            <div class="col-md-6">
                                <input id="licenseKey" type="licenseKey" class="form-control @error('licenseKey') is-invalid @enderror" value="" name="licenseKey"  >

                                @error('licenseKey')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                          <div class="form-group row">
                            <label for="licenseKey" class="col-md-4 col-form-label text-md-right"></label>

                            <div class="col-md-6">
                                <button onclick="myFunction()" style="width: 328px" type="submit" class="btn btn-info">
                                    {{ __('Save') }}
                                </button>
                            </div>
                        </div>



                        <div class="form-group row mb-0">
                            <div class="col-md-4 offset-md-8">
                                <button type="submit" class=".btn-default">
                                    {{ __('Create Key') }}
                                   
                                </button><br>
                                 {{ __('Return to Login Page') }}

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
