@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">

                </div>

                <div class="card-body" style="background-color: #9cc16d">
                    <form method="POST" action="{{ route('license.key_save') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="clientID" class="col-md-4 col-form-label text-md-right">{{ __('Client ID') }}</label>

                            <div class="col-md-6">
                                <input id="clientID" type="clientID" class="form-control @error('clientID') is-invalid @enderror" name="clientID" value="{{$userDetails->user_id}}" required autocomplete="clientID" autofocus disabled="">

                                @error('clientID')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="licenseKey" class="col-md-4 col-form-label text-md-right">{{ __('License Key') }}</label>

                            <div class="col-md-6">
                                <input id="licenseKey" type="licenseKey" class="form-control @error('licenseKey') is-invalid @enderror" value="{{$key}}" name="licenseKey"  >

                                @error('licenseKey')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                          <div class="form-group row">
                            <label for="licenseKey" class="col-md-4 col-form-label text-md-right"></label>

                            <div class="col-md-6">
                                <button onclick="myFunction()" style="width: 328px" type="submit" class="btn btn-info">
                                    {{ __('Save') }}
                                </button>
                            </div>
                        </div>




                        <div class="form-group row">
                            <label for="licenseKey" class="col-md-4 col-form-label text-md-right">{{ __('License For') }}</label>

                            <div class="col-md-2">
                              
                          </div>
                            <div class="col-md-2">
                               <div class="form-group">
                              <select class="form-control" id="sel1" name="exp_date" style="width:112px;" disabled>
                                <option value="3">3</option>
                                <option value="6">6</option>
                                <option value="12">12</option>
                                
                              </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                          <p>Months</p>    
                        </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-4 offset-md-8">
                                <button type="submit" class=".btn-default">
                                    {{ __('Create Key') }}
                                   
                                </button><br>
                                 {{ __('Return to Login Page') }}

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function myFunction() {
  alert("Congratulations!! Your License Has Been Activated. It will work till {{$userDetails->exp_date}} ");
}
</script>
@endsection
