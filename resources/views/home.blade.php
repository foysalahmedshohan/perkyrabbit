@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Create License') }}</div>

                <div class="card-body" style="background-color: #9cc16d">
                    <form method="POST" action="{{ route('license.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="user_id" class="col-md-4 col-form-label text-md-right">{{ __('Client ID') }}</label>

                            <div class="col-md-6">
                                <input id="user_id" type="user_id" class="form-control @error('user_id') is-invalid @enderror" name="user_id" value="{{ old('user_id') }}" required autocomplete="user_id" autofocus>

                                @error('user_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="licenseKey" class="col-md-4 col-form-label text-md-right">{{ __('License Key') }}</label>

                            <div class="col-md-6">
                                <input id="" type="licenseKey" class="form-control " name="licenseKey" >

                            </div>
                        </div>


                          <div class="form-group row">
                            <label for="licenseKey" class="col-md-4 col-form-label text-md-right"></label>

                            <div class="col-md-6">
                                <button style="width: 328px" type="submit" class="btn btn-info">
                                    {{ __('Save') }}
                                </button>
                            </div>
                        </div>




                        <div class="form-group row">
                            <label for="licenseKey" class="col-md-4 col-form-label text-md-right">{{ __('License For') }}</label>

                            <div class="col-md-2">
                              
                          </div>
                            <div class="col-md-2">
                               <div class="form-group">
                              <select class="form-control" id="sel1" style="width:112px;">
                                <option>3</option>
                                <option>6</option>
                                <option>12</option>
                                
                              </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                          <p>Months</p>    
                        </div>
                        </div>



                        <div class="form-group row mb-0">
                            <div class="col-md-4 offset-md-8">
                                <button type="submit" class=".btn-default">
                                    {{ __('Create Key') }}
                                   
                                </button><br>
                                 {{ __('Return to Login Page') }}

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
