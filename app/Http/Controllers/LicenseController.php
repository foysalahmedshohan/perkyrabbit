<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;

class LicenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('license.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('license.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $id=Auth::user()->id;
        if($user=User::where('id',$id)->first()){           
            $user->user_id=$request->user_id;
            $user->save();
        } 
        $userDetails=User::where('id',$id)->first();
        return view('license.index', compact('userDetails'))->with('value', 1);                  
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



      public function exp_date(Request $request)
    {

     $key= mt_getrandmax();
     $currentDateTime = Carbon::now();
     $newDateTime = Carbon::now()->addMonth($request->exp_date);
     //return $newDateTime;

       $id=Auth::user()->id;
        if($user=User::where('id',$id)->first()){           
            $user->exp_date=$newDateTime;
            $user->save();
        } 
        $userDetails=User::where('id',$id)->first();
        return view('license.date', compact('userDetails','key'))->with('value', 1);

    }


      public function key_save(Request $request)
    {
        $id=Auth::user()->id;
        if($user=User::where('id',$id)->first()){           
            $user->license_key=$request->licenseKey;
            $user->save();
        } 
        $userDetails=User::where('id',$id)->first();
        return view('license.updateKey', compact('userDetails','key'))->with('value', 1);       
    }


      public function key_update(Request $request)
    {
      // return $request;
        $id=Auth::user()->id;
        if($user=User::where('id',$id)->first()){           
            $user->license_key=$request->licenseKey;
            $user->save();
        } 
        $userDetails=User::where('id',$id)->first();
        return view('license.index', compact('userDetails','key'));       
    }




}
